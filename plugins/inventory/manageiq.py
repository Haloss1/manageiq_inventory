#!/usr/bin/python
# -*- coding: utf-8 -*-#

# Copyright (C) 2022 Andrew Montoya <halossqwerty@gmail.com> AKA Andrew Montoya <amontoya@whitestack.com>
# Copyright (C) 2016 Guido Günther <agx@sigxcpu.org>
# GNU General Public License v3.0+ (see https://www.gnu.org/licenses/gpl-3.0.txt)

DOCUMENTATION = '''
---
name: manageiq
plugin_type: inventory
author: Andrew Montoya (@haloss1) <halossqwerty@gmail.com>
version_added: "0.1.0"
short_description: ManageIQ inventory source
description:
  - Get inventory hosts from a ManageIQ instance.
  - "Uses a configuration file as an inventory source, it must end in C(.manageiq.yml) or C(.manageiq.yaml)"
options:
  plugin:
    description: Name of the plugin
    required: true
    choices: ["haloss1.manageiq_inventory.manageiq"]
    type: str
  manageiq_version:
    description: ManageIQ version used
    required: true
    type: float
  url:
    description: URL to the ManageIQ instance
    required: true
    type: str
  username:
    description: ManageIQ username
    required: true
    type: str
  password:
    description: ManageIQ password
    required: true
    type: str
  ssl_verify:
    description: Verify SSL certificate
    default: true
    type: bool
  limit:
    description: "Limit the number of VMs returned per request (0 means unlimited)"
    default: 0
    type: int
  purge_actions:
    description: Purge the ManageIQ actions from hosts
    default: true
    type: bool
  clean_group_keys:
    description: "Clean up group names (from tags and other groupings so Ansible doesn't complain)"
    default: true
    type: bool
  nest_tags:
    description: "Explode tags into nested groups / subgroups"
    default: false
    type: bool
  suffix:
    description: Suffix for hostnames
    type: str
  prefer_ipv4:
    description: "Use an IPv4 address for the ansible_ssh_host rather than just the first IP address in the list (Only use this if you have a broken IPv6 / Dual Stack setup)"
    default: false
    type: bool
  debug:
    description: Debug output
    default: false
    type: bool
'''

from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable
from ansible.errors import AnsibleError, AnsibleParserError
import requests
from requests.auth import HTTPBasicAuth
import json
import warnings
import re


class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):
    NAME = "haloss1.manageiq_inventory.manageiq"

    def verify_file(self, path):
        """Return true/false if this is possibly a valid file for this plugin to consume"""
        valid = True
        return valid

    def to_safe(self, word):
        """
        Converts 'bad' characters in a string to underscores so they can be used as Ansible groups
        """
        if self.manageiq_clean_group_keys:
            regex = r"[^A-Za-z0-9\_]"
            return re.sub(regex, "_", word.replace(" ", ""))
        else:
            return word

    def _http_request(self, url):
        """
        Make a request and return the result converted from JSON
        """
        results = []

        ret = requests.get(
            url,
            auth=HTTPBasicAuth(self.manageiq_username, self.manageiq_pw),
            verify=self.manageiq_ssl_verify,
        )

        ret.raise_for_status()

        try:
            results = json.loads(ret.text)
        except ValueError:
            warnings.warn(
                "Unexpected response from {0} ({1}): {2}".format(
                    self.manageiq_url, ret.status_code, ret.reason
                )
            )
            results = {}

        if self.manageiq_debug:
            print(
                "======================================================================="
            )
            print(
                "======================================================================="
            )
            print(
                "======================================================================="
            )
            print(ret.text)
            print(
                "======================================================================="
            )
            print(
                "======================================================================="
            )
            print(
                "======================================================================="
            )

        return results

    def _get_json(self, endpoint, url_suffix):
        """
        Make a request by given url, split request by configured limit,
        go through all sub-requests and return the aggregated data received
        by manageiq

        :param endpoint: api endpoint to access
        :param url_suffix: additional api parameters

        """

        limit = int(self.manageiq_limit)

        page = 0
        last_page = False

        results = []

        while not last_page:
            offset = page * limit
            url = "%s%s?offset=%s&limit=%s%s" % (
                self.manageiq_url,
                endpoint,
                offset,
                limit,
                url_suffix,
            )

            if self.manageiq_debug:
                print("Connecting to url '%s'" % url)

            ret = self._http_request(url)
            results += [ret]

            if "subcount" in ret:
                if ret["subcount"] < limit:
                    last_page = True
                page += 1
            else:
                last_page = True

        return results

    def _get_hosts(self):
        """
        Get all hosts
        """
        endpoint = "/api/vms"
        url_suffix = "&expand=resources,tags,hosts,&attributes=active,ipaddresses&filter[]=active=true"
        results = self._get_json(endpoint, url_suffix)
        resources = [item for sublist in results for item in sublist["resources"]]

        return resources

    def parse(self, inventory, loader, path, cache):
        """Return dynamic inventory from source"""
        super(InventoryModule, self).parse(inventory, loader, path, cache)
        # Read the inventory YAML file
        self._read_config_data(path)
        try:
            # Store the options from the YAML file
            self.manageiq_url = self.get_option("url")
            self.manageiq_limit = self.get_option("limit")
            self.manageiq_username = self.get_option("username")
            self.manageiq_pw = self.get_option("password")
            self.manageiq_ssl_verify = self.get_option("ssl_verify")
            self.manageiq_debug = self.get_option("debug")
            self.manageiq_clean_group_keys = self.get_option("clean_group_keys")
        except Exception as e:
            raise AnsibleParserError("All correct options required: {}".format(e))
        #  Call our internal helper to populate the dynamic inventory
        for resource in self._get_hosts():
            if resource.get("power_state") == "on":
                self.inventory.add_group(resource["vendor"])
                self.inventory.add_host(resource["name"], group=resource["vendor"])
                if len(resource.get("tags", [])) > 0:
                    for group in resource["tags"]:
                        self.inventory.add_group(self.to_safe(group["name"][1:].split("/")[-1]))
                        self.inventory.add_host(
                            resource["name"],
                            group=self.to_safe(group["name"][1:].split("/")[-1]),
                        )
                self.inventory.set_variable(
                    resource["name"], "ansible_host", resource["ipaddresses"][0] # TODO: Find a way to use all the IP addresses from the host (Functions as fallback), or at least make it configurable instead of hardcoding the first one returned by the ManageIQ API.
                )
